@ECHO OFF
setlocal enabledelayedexpansion
ECHO Script adding folder name to filename for files with extensions
ECHO set in variable "ext".
ECHO Script is working recursive, look through all subfolders.
ECHO {FILE_NAME}.{FILE_EXTENSION}  ---^>  {FOLDER}_{FILE_NAME}.{FILE_EXTENSION}
ECHO Marek Szczepkowski
ECHO Date: 23.11.2022
ECHO Version: 1.0

REM ALLOWED EXTENSIONS LIST
SET ext=.tiff,.tif,.tfw,.ecw

REM PROMPT - ADD PATH
ECHO. 
set /P WORK="Please add input path with files or subfolders: "

REM PROMPT - CONTINUE
ECHO.
ECHO %WORK%
set yesList=(Y,y,Yes,YES,Yes,yes)
set /p var=Are you sure the input path is correct?[Y/N]:

for %%z in %yesList% do (
	if "%var%"=="%%z" goto PROCESS)
	
ECHO.
ECHO Process aborted
pause
exit




:PROCESS
REM PROCESS
ECHO.
ECHO. 
ECHO START RENAMING...
ECHO.

REM LISTIN FOLDER WITH RECURSIVE
for /F "delims=" %%G in ('dir /B /S "%WORK%\*"') do (
REM LOOP THROUGH FILE LIST
    for %%g in ("%%~dpG.") do (
REM LOOP THROUGH FILE EXTENSIONS
		FOR %%e IN (%ext%) do (
REM CHECK WITH ALLOWED EXTENSIONS EXT		
			if %%e==%%~xG (
REM LOOP THROUGH FILES AND RENAME
				for %%g in ("%%~dpG.") do (
					ECHO.
					ECHO SUBFOLDER: %%~dpG
					ECHO RENAMING:  %%~nxG  --^>  %%~nxg_%%~nxG
					rename "%%~fG" "%%~nxg_%%~nxG"
					)
				)
			)
		)
	)
)
ECHO.
ECHO.
ECHO PROCESS ENDS WITHOUT ANY ERRORS
pause
